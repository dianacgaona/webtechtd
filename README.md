# Web Tech with Angular

- Using HTML, CSS and especially ECMAScript in Angular
- Useful URLs are in the wiki http://wiki.conygre.com/doku.php?id=prod-support:angular

## Overview
### Day 1
- Overview of the technologies
- begin using Angular and the Angular command-line interface (cli)
- 2:00pm Guest Speaker - Annu Gulati: UI Design Principles at Citi
- Workshop

### Day 2
- Architecture
- Consuming external data over http(s)
- Unit testing
- Workshop

### Day 3
- Production builds and deployment
- ES6 features
- Assessed workshop
---
Mostly we will build components and services with hands-on coding exercises. Almost exclusively I'll be using Visual Studio Code. We will use the tool-chain provided with Angular for rapid development

There are very few dependencies. Unless I specify otherwise, you can code however you like and vary, extend or leave out my examples. All the code you see me build will be made available.
